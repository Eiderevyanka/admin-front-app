package ru.reboot.controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.reboot.dto.ItemInfo;

import java.util.List;

public interface AdminController {

    /**
     * Get all items.
     */
    List<ItemInfo> getAllItems();

    /**
     * Set item price.
     *
     * @param itemId - item id
     * @param price  - new price
     */
    void setPrice(String itemId, double price) throws Exception;

    /**
     * Delete item price.
     *
     * @param itemId - item id
     */
    @DeleteMapping(value = "item/price")
    void deletePrice(@RequestParam("itemId") String itemId) throws Exception;
}
