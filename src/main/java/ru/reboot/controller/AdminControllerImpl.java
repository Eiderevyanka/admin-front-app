package ru.reboot.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.reboot.dto.ItemDTO;
import ru.reboot.dto.ItemInfo;
import ru.reboot.error.BusinessLogicException;
import ru.reboot.service.AdminService;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Price controller.
 */
@RestController
@RequestMapping(path = "admin")
public class AdminControllerImpl implements AdminController {

    private static final Logger logger = LogManager.getLogger(AdminControllerImpl.class);

    private AdminService adminService;

    @Autowired
    public void setAdminService(AdminService adminService) {
        this.adminService = adminService;
    }

    @GetMapping("info")
    public String info() {
        logger.info("method .info invoked");
        return "AdminController " + new Date();
    }

    @Override
    @GetMapping("item/all")
    public List<ItemInfo> getAllItems() {
        return adminService.getAllItems();
    }

    @Override
    @PutMapping(value = "item/price")
    public void setPrice(@RequestParam("itemId") String itemId, @RequestParam("price") double price) throws Exception {
        adminService.setPrice(itemId, price);
    }

    @Override
    @DeleteMapping(value = "item/price")
    public void deletePrice(@RequestParam("itemId") String itemId) throws Exception {
        adminService.deletePrice(itemId);
    }

    @PostMapping(value = "item")
    public ItemDTO createItem(@RequestBody ItemDTO item) throws Exception {
        return adminService.createItem(item);
    }

    @ExceptionHandler(BusinessLogicException.class)
    public ResponseEntity<Map<String, String>> handleException(BusinessLogicException ex, HttpServletRequest request) {

        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        Map<String, String> map = new HashMap<>();

        map.put("errorCode", ex.getCode());
        map.put("message", ex.getMessage());
        map.put("timestamp", LocalDateTime.now().toString());
        map.put("status", String.valueOf(status.value()));
        map.put("error", status.getReasonPhrase());
        map.put("path", request.getRequestURI());

        return new ResponseEntity<>(map, status);
    }
}
