package ru.reboot.dto;

import java.time.LocalDateTime;

public class PriceInfo {

    private String itemId;
    private Double price;
    private String lastAccessTime;

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getLastAccessTime() {
        return lastAccessTime;
    }

    public void setLastAccessTime(String lastAccessTime) {
        this.lastAccessTime = lastAccessTime;
    }

    @Override
    public String toString() {
        return "PriceInfo{" +
                "itemId='" + itemId + '\'' +
                ", price=" + price +
                ", lastAccessTime=" + lastAccessTime +
                '}';
    }
}
