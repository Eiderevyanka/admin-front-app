
package ru.reboot.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.*;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RequestCallback;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;
import ru.reboot.dto.*;
import ru.reboot.error.BusinessLogicException;
import ru.reboot.error.ErrorCode;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class AdminService {

    private static final Logger logger = LogManager.getLogger(AdminService.class);

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Value("${client.stock-service}")
    private String stockServiceUrl;

    @Value("${client.price-service}")
    private String priceServiceUrl;

    private ObjectMapper mapper;
    private ItemCache itemCache;
    private PriceCache priceCache;
    private RestTemplate restTemplate;

    @Autowired
    public void setMapper(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Autowired
    public void setItemCache(ItemCache itemCache) {
        this.itemCache = itemCache;
    }

    @Autowired
    public void setPriceCache(PriceCache priceCache) {
        this.priceCache = priceCache;
    }

    /**
     * Set item price.
     * We should send request to price-service
     *
     * @param itemId - item id
     * @param price  - item price
     */
    public void setPrice(String itemId, double price) throws Exception {
        logger.info("method .setPrice");
        try {
            validateItemId(itemId);

            PriceInfo priceInfo = new PriceInfo();
            priceInfo.setItemId(itemId);
            priceInfo.setPrice(price);
            priceInfo.setLastAccessTime(LocalDateTime.now().toString());

            mapper.registerModule(new JavaTimeModule());
            mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
            String json = mapper.writeValueAsString(priceInfo);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<String> request = new HttpEntity<>(json, headers);
            restTemplate.put(priceServiceUrl + "/price", request);

            logger.info("Method .setPrice completed, itemId={}, price={}", itemId, price);
        } catch (Exception ex) {
            logger.error("Failed to .setPrice error={}", ex.toString(), ex);
            throw ex;
        }
    }

    /**
     * Delete item price.
     * We should send request to price-service
     *
     * @param itemId - item id
     */
    public void deletePrice(String itemId) throws Exception {
        logger.info("method .deletePrice");
        try {
            validateItemId(itemId);

            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<String> request = new HttpEntity<>(headers);
            restTemplate.delete(priceServiceUrl + "/price?itemId=" + itemId, request);

            logger.info("Method .deletePrice completed, itemId={}", itemId);
        } catch (Exception ex) {
            logger.error("Failed to .deletePrice error={}", ex.toString(), ex);
            throw ex;
        }
    }

    /**
     * Get all items.
     */
    public List<ItemInfo> getAllItems() {
        logger.info("method .getAllItems");

        try {
            List<ItemInfo> itemInfoList = itemCache.values().stream()
                    .map(item -> new ItemInfo(item, priceCache.get(item.getId())))
                    .collect(Collectors.toList());

            logger.info("Method .getAllItems completed, itemInfoList={}", itemInfoList);
            return itemInfoList;
        } catch (Exception e) {
            logger.error("Failed to .getAllItems error={}", e.toString(), e);
            throw e;
        }
    }

    /**
     * Create item in stock-service
     *
     * @param item - created item
     */
    public ItemDTO createItem(ItemDTO item) throws Exception {

        try {
            logger.info("method .createItem");

            if (Objects.isNull(item) || Objects.isNull(item.getId()) || item.getId().equals("")) {
                throw new BusinessLogicException("Bad data", ErrorCode.ILLEGAL_ARGUMENT);
            }

            String json = mapper.writeValueAsString(item);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);

            ItemDTO createdItem;

            try {
                createdItem = restTemplate.postForObject(stockServiceUrl + "/stock/item",
                        new HttpEntity<>(json, headers), ItemDTO.class);
            } catch (HttpServerErrorException.InternalServerError ex) {
                Map<String, String> map = mapper.readValue(ex.getResponseBodyAsString(), Map.class);
                if (map.containsKey("errorCode")) {
                    throw new BusinessLogicException(map.get("message"), map.get("errorCode"));
                } else {
                    throw ex;
                }
            }


            logger.info("Method .createItem completed, ItemDTO={}", createdItem);
            return createdItem;
        } catch (Exception ex) {
            logger.error("Failed to .createItem error={}", ex.toString(), ex);
            throw ex;
        }
    }

    @KafkaListener(topics = "ITEM_CHANGED_EVENT", groupId = "admin-front-app", autoStartup = "${kafka.autoStartup}")
    public void onItemChangedEventListener(String raw) {
        logger.info("method .onItemChangedEventListener");
        try {
            ItemChangedEvent event = mapper.readValue(raw, ItemChangedEvent.class);
            logger.info("<< Received: {}", raw);

            String itemId = event.getItem().getId();
            ItemDTO item = event.getItem();

            if (event.getEventType().equals(ItemChangedEvent.Type.UPDATE)) {
                itemCache.put(itemId, item);
                logger.info("Method .onItemChangedEventListener completed, put itemId={}, item={}",
                        itemId, item);
            } else {
                itemCache.remove(itemId);
                logger.info("Method .onItemChangedEventListener completed, remove itemId={}", itemId);
            }

        } catch (JsonProcessingException e) {
            logger.error("Failed to .onItemChangedEventListener error={}", e.toString(), e);
        }
    }

    @KafkaListener(topics = "PRICE_CHANGED_EVENT", groupId = "admin-front-app", autoStartup = "${kafka.autoStartup}")
    public void onPriceChangedEventListener(String raw) {
        logger.info("method .onPriceChangedEventListener");
        try {
            PriceChangedEvent event = mapper.readValue(raw, PriceChangedEvent.class);
            logger.info("<< Received: {}", raw);

            String itemId = event.getPrice().getItemId();
            Double price = event.getPrice().getPrice();

            if (event.getEventType().equals(PriceChangedEvent.Type.UPDATE)) {
                priceCache.putPrice(itemId, price);
                logger.info("Method .onPriceChangedEventListener completed, put itemId={}, price={}",
                        itemId, price);
            } else {
                priceCache.removePrice(itemId);
                logger.info("Method .onPriceChangedEventListener completed, remove itemId={}", itemId);
            }

        } catch (Exception e) {
            logger.error("Failed to .onPriceChangedEventListener error={}", e.toString(), e);
        }
    }

    /**
     * Init cache here.
     */
    @PostConstruct
    public void init() throws Exception {

        initializePriceCache();
        initializeItemCache();
    }

    private void initializePriceCache() throws Exception {
        logger.info("Method .initializePriceCache started");

        try {
            RestTemplate restTemplate = new RestTemplate();

            String json = restTemplate.getForObject(priceServiceUrl + "/price/all", String.class);
            List<PriceInfo> priceInfoList = mapper.readValue(json, new TypeReference<List<PriceInfo>>() {
            });

            priceInfoList.forEach(priceInfo ->
                    priceCache.putPrice(priceInfo.getItemId(), priceInfo.getPrice()));

        } catch (Exception ex) {
            logger.error("Failed to .initializePriceCache error={}", ex.toString(), ex);
            throw ex;
        }
        logger.info("Method .initializePriceCache completed");
    }

    private void initializeItemCache() {

        logger.info("method .initializeItemCache");

        RestTemplate restTemplate = new RestTemplate();

        try {
            ItemDTO[] items = restTemplate.getForObject(stockServiceUrl + "/stock/item/all", ItemDTO[].class);

            for (ItemDTO item : items) {
                itemCache.put(item.getId(), item);
            }
            logger.info("Method .initializeItemCache completed");

        } catch (Exception e) {
            logger.error("Failed to .initializeItemCache error={}", e.toString(), e);
            throw new BusinessLogicException(e.getMessage(), ErrorCode.ILLEGAL_ARGUMENT);
        }
    }

    private void validateItemId(String itemId) {
        if (Objects.isNull(itemId) || itemId.equals("")) {
            throw new BusinessLogicException("Error empty parameter", ErrorCode.ILLEGAL_ARGUMENT);
        }
    }
}
