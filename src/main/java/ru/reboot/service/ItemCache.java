package ru.reboot.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;
import ru.reboot.dto.ItemDTO;
import ru.reboot.error.BusinessLogicException;
import ru.reboot.error.ErrorCode;

import java.util.*;

/**
 * Item cache. Loads on startup
 */
@Component
public class ItemCache {

    private static final Logger logger = LogManager.getLogger(ItemCache.class);
    private final Map<String, ItemDTO> itemInfoMap = new HashMap<>();

    /**
     * Get item by item id.
     *
     * @param itemId - item id
     * @return item or null if item doesn't exists
     */
    public ItemDTO get(String itemId) {
        logger.info("method .get");

        try {
            if (itemId == null || itemId.equals("")) {
                return null;
            }

            ItemDTO item = itemInfoMap.get(itemId);
            logger.info("Method .get completed, item={}", item);
            return item;

        } catch (Exception e) {
            logger.error("Failed to .get error={}", e.toString(), e);
            throw e;
        }
    }

    /**
     * Put {@link ItemDTO} into map by item id.
     *
     * @param itemId   - item id
     * @param itemInfo - ItemInfo
     * @throws {@link BusinessLogicException} if itemId is null
     */
    public void put(String itemId, ItemDTO itemInfo) {
        logger.info("method .put");

        try {
            if (itemId == null || itemId.equals("")) {
                throw new BusinessLogicException("itemId can't be null", ErrorCode.ILLEGAL_ARGUMENT);
            }

            itemInfoMap.put(itemId, itemInfo);
            logger.info("Method .put completed, itemId={}", itemId);
        } catch (Exception e) {
            logger.error("Failed to .put error={}", e.toString(), e);
            throw e;
        }
    }

    /**
     * Get keys from map
     *
     * @return KeySet
     */
    public Set<String> getKeys() {
        logger.info("method .getKeys");

        try {
            Set<String> keys = itemInfoMap.keySet();
            logger.info("Method .getKeys completed, keys={}", keys);
            return keys;
        } catch (Exception e) {
            logger.error("Failed to .getKeys error={}", e.toString(), e);
            throw e;
        }
    }

    /**
     * Get values from map
     *
     * @return List of {@link ItemDTO}
     */
    public List<ItemDTO> values() {
        logger.info("method .values");

        try {
            List<ItemDTO> values = new ArrayList<>(itemInfoMap.values());
            logger.info("Method .values completed, values={}", values);
            return values;

        } catch (Exception e) {
            logger.error("Failed to .values error={}", e.toString(), e);
            throw e;
        }
    }

    /**
     * Remove item from map
     *
     * @param itemId
     * @throws {@link BusinessLogicException} if itemId is null
     */
    public void remove(String itemId) {
        logger.info("method .remove");

        try {
            if (itemId == null || itemId.equals("")) {
                throw new BusinessLogicException("itemId can't be null", ErrorCode.ILLEGAL_ARGUMENT);
            }
            itemInfoMap.remove(itemId);
            logger.info("Method .remove completed, itemId={}", itemId);

        } catch (Exception e) {
            logger.error("Failed to .remove error={}", e.toString(), e);
            throw e;
        }
    }
}
