package ru.reboot.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;
import ru.reboot.error.BusinessLogicException;
import ru.reboot.error.ErrorCode;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

/**
 * Item cache. Loads on startup.
 */
@Component
public class PriceCache {

    private static final Logger logger = LogManager.getLogger(PriceCache.class);
    private Map<String, Double> cache = new HashMap<>();

    /**
     * Get item by item id.
     *
     * @param itemId - item id
     * @return item or null if item doesn't exists
     */
    public Double get(String itemId) {
        logger.info("method .get");

        try {
            if (itemId == null || itemId.equals("")) {
                return null;
            }
            Double price = cache.get(itemId);
            logger.info("Method .get completed, price={}", price);
            return price;

        } catch (Exception e) {
            logger.error("Failed to .get error={}", e.toString(), e);
            throw e;
        }
    }

    /**
     * Check if item has assigned price.
     *
     * @param itemId - item id
     * @return true if itemId exists in map
     */
    public boolean contains(String itemId) {
        logger.info("method .contains");
        try {
            boolean contains = cache.containsKey(itemId);
            logger.info("Method .contains completed, contains={}", contains);
            return contains;

        } catch (Exception e) {
            logger.error("Failed to .contains error={}", e.toString(), e);
            throw e;
        }
    }

    /**
     * Put price in cache.
     *
     * @param itemId - item id
     * @param price  - price
     * @throws {@link BusinessLogicException} if itemId is null
     */
    public void putPrice(String itemId, double price) {
        logger.info("method .putPrice");

        try {
            if (itemId == null || itemId.equals("")) {
                throw new BusinessLogicException("itemId can't be null", ErrorCode.ILLEGAL_ARGUMENT);
            }
            cache.put(itemId, price);
            logger.info("Method .put completed, itemId={}", itemId);
        } catch (Exception e) {
            logger.error("Failed to .put error={}", e.toString(), e);
            throw e;
        }
    }

    /**
     * Remove price from cache.
     *
     * @param itemId - item id
     * @throws {@link BusinessLogicException} if itemId is null
     */
    public void removePrice(String itemId) {
        logger.info("method .removePrice");

        try {
            if (itemId == null || itemId.equals("")) {
                throw new BusinessLogicException("itemId can't be null", ErrorCode.ILLEGAL_ARGUMENT);
            }
            cache.remove(itemId);
            logger.info("Method .removePrice completed, itemId={}", itemId);

        } catch (Exception e) {
            logger.error("Failed to .removePrice error={}", e.toString(), e);
            throw e;
        }
    }
}
