package ru.reboot.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.reboot.dto.User;
import ru.reboot.dto.UserPrincipal;


@Service
public class UserService implements UserDetailsService {

    private static final Logger logger = LogManager.getLogger(UserService.class);

    @Value("${client.user-service}")
    private String userServiceUrl;

    private ObjectMapper mapper;

    @Autowired
    public void setMapper(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        logger.info("Method .loadUserByUsername started");

        try {
            RestTemplate restTemplate = new RestTemplate();

            String json = restTemplate.getForObject(
                    userServiceUrl + "/users/user/byLogin?login=" + login, String.class
            );
            User user = mapper.readValue(json, User.class);
            if (user == null) {
                throw new UsernameNotFoundException(login);
            }

            user.setPassword(passwordEncoder.encode(user.getPassword()));
            logger.info("Method .loadUserByUsername completed");

            return new UserPrincipal(user);
        } catch (UsernameNotFoundException ex){
            logger.error("Failed to .loadUserByUsername error={}", ex.toString(), ex);
            throw ex;
        }
        catch (Exception ex) {
            logger.error("Failed to .loadUserByUsername error={}", ex.toString(), ex);
        }
        return null;
    }
}
