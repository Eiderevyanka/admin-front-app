function showPreMainPage() {

    let mainPage = "<div class='main'><h1>Административная панель SMarket</h1>";

    mainPage += "<img class='item' src='images/newItem.png' alt='image' onClick='showCreateItemPage()'>"
        + "<img class='item' src='images/setPrice.png' alt='image' onClick='showMainPage()'></div>"

    document.getElementById("root").innerHTML = mainPage
}

function showMainPage() {

    let mainPage = "<h1>Административная панель SMarket</h1>";

    let items = JSON.parse(getPriceList());

    mainPage += "<div class='main'><table class='table'>"

    mainPage += getHeader()
    mainPage += "<tbody>"

    for (let i = 0; i < items.length; ++i) {
        mainPage += createRow(i + 1, items[i])
    }``

    mainPage += "</tbody>"
    mainPage += "</table>"
    mainPage += "<br><button class='link' onclick='showPreMainPage()'>Назад</button></div>"

    document.getElementById("root").innerHTML = mainPage
}

function getHeader() {

    return "<thead><tr>"
        + "<th>№</th>"
        + "<th>Id</th>"
        + "<th>Название</th>"
        + "<th>Производитель</th>"
        + "<th>Остаток</th>"
        + "<th>Закупочная цена</th>"
        + "<th>Цена</th>"
        + "</tr></thead>"
}

function getPriceList() {
    let http = new XMLHttpRequest();
    let url = '/admin/item/all'
    http.open('GET', url, false)
    http.send(null)

    return http.responseText
}

function createRow(idx, item) {

    let rowId = "row_" + idx
    let price = (item.price == null) ? "" : item.price

    return "<tr id='" + rowId + "'>"
        + "<td>" + idx + "</td>"
        + "<td>" + item.id + "</td>"
        + "<td>" + item.name + "</td>"
        + "<td>" + item.manufacturer + "</td>"
        + "<td>" + item.count + "</td>"
        + "<td>" + item.purchasePrice + "</td>"
        + "<td><input class='priceInput' value=" + price + " ></td>"
        + "<td><input type='button' value='Назначить' onClick=onAssignPriceButtonClicked('" + rowId + "')></td>"
        + "<td><input type='button' value='Удалить' onClick=onDeletePriceButtonClicked('" + rowId + "')></td>"
        + "</tr>"
}

function onAssignPriceButtonClicked(rowId) {

    let itemId = document.querySelector("#" + rowId + " > td:nth-child(2)").textContent
    let newPrice = document.querySelector("#" + rowId + "  .priceInput").value

    if (newPrice == null || isNaN(newPrice) || newPrice === "") {
        alert('Price is not specified')
        return
    }

    alert('Assign price itemId=' + itemId + " price=" + newPrice)

    var http = new XMLHttpRequest()
    var url = '/admin/item/price?itemId=' + itemId + "&price=" + newPrice
    http.open('PUT', url, false)
    http.send(null)

    showMainPage()
}

function onDeletePriceButtonClicked(rowId) {

    let itemId = document.querySelector("#" + rowId + " > td:nth-child(2)").textContent

    alert('Delete price itemId=' + itemId)

    let http = new XMLHttpRequest();
    let url = '/admin/item/price?itemId=' + itemId
    http.open('DELETE', url, false)
    http.send(null)

    showMainPage()
}

function showCreateItemPage() {
    let mainPage = "<div class='main'><h1>Заведение нового товара в базу данных</h1>";

    mainPage += "<table>"
        + "<tbody>"
        + "<tr>"
        + "<th>Параметр</th>"
        + "<th>Значение</th>"
        + "</tr>"
        + "<tr>"
        + "<td>ID</td>"
        + "<td><input id='id' value='' color = 'black' required/></td>"
        + "</tr>"
        + "<tr>"
        + "<td>Название</td>"
        + "<td><input id='name' value='' required/></td>"
        + "</tr>"
        + "<tr>"
        + "<td>Сокращенное название</td>"
        + "<td><input id='short_name' value='' required/></td>"
        + "</tr>"
        + "<tr>"
        + "<td>Описание</td>"
        + "<td><textarea id='description'></textarea></td>"
        + "</tr>"
        + "<tr>"
        + "<td>Единица измерения</td>"
        + "<td><input id='unit' value='' required/></td>"
        + "</tr>"
        + "<tr>"
        + "<td>Закупочная цена</td>"
        + "<td><input id='price' value='' type='number' min='0' step='0.01' required/></td>"
        + "</tr>"
        + "<tr>"
        + "<td>Категория товара</td>"
        + "<td><select id='category' required>\n" +
        "  <option value='FRUITS' selected>Фрукты</option>\n" +
        "  <option value='VEGETABLES'>Овощи</option>\n" +
        "  <option value='CHEESES'>Сыры</option>\n" +
        "  <option value='CANDIES'>Конфеты</option>\n" +
        "  <option value='SAUSAGES'>Колбасы</option>\n" +
        "  <option value='CANNEDFOOD'>Консервы</option>\n" +
        "  <option value='BAKERY'>Хлебобулочные изделия</option>\n" +
        "  <option value='ALCOHOL'>Алкоголь</option>\n" +
        "</select></td>"
        + "</tr>"
        + "<tr>"
        + "<td>Производитель</td>"
        + "<td><input id='manufacturer' value='' required/></td>"
        + "</tr>"
        + "<tr>"
        + "<td>Количество</td>"
        + "<td><input id='count' value='' type='number' min='0' required/></td>"
        + "</tr>"
        + "<tr>"
        + "<td></td>"
        + "<td>"
        + "<button onclick='onCreateItemButtonClicked()'>Создать</button>"
        + "</td>"
        + "</tr>"
        + "</tbody>"
        + "</table>"
        + "<br><button class='link' onclick='showPreMainPage()'>Назад</button>"
        + "</div>"

    document.getElementById("root").innerHTML = mainPage

}

function validateItem() {

    let item = {
        id: document.getElementById('id').value,
        name: document.getElementById('name').value,
        short_name: document.getElementById('short_name').value,
        description: document.getElementById('description').value,
        unit: document.getElementById('unit').value,
        price: document.getElementById('price').value,
        category: document.getElementById('category').value,
        manufacturer: document.getElementById('manufacturer').value,
        count: document.getElementById('count').value,
    }

    if (isNaN(item.price) || item.price < 0) {
        alert('В поле "Закупочная цена" необходимо ввести положительное число')
        document.getElementById('price').style.color = "red"
        document.getElementById('price').style.background = "LightYellow"
        return false
    }

    if (isNaN(item.count) || item.count < 0) {
        alert('В поле "Количество" необходимо ввести положительное число')
        document.getElementById('count').style.color = "red"
        document.getElementById('count').style.background = "LightYellow"
        return false
    }

    document.getElementById('price').style.color = ""
    document.getElementById('price').style.background = ""
    document.getElementById('count').style.color = ""
    document.getElementById('count').style.background = ""

    let isEmptyFields = false

    for (let param in item) {
        if (document.getElementById(param).value === '') {
            document.getElementById(param).placeholder = 'Заполните это поле'
            isEmptyFields = true
        }
    }

    if (isEmptyFields) {
        alert("Необходимо заполнить все поля")
        return false
    }

    return true
}

function onCreateItemButtonClicked() {
    if (!validateItem()) {
        return
    }
    let id = document.getElementById('id').value;
    let name = document.getElementById('name').value;
    let short_name = document.getElementById('short_name').value;
    let description = document.getElementById('description').value;
    let unit = document.getElementById('unit').value;
    let price = document.getElementById('price').value;
    let category = document.getElementById('category').value;
    let manufacturer = document.getElementById('manufacturer').value;
    let count = document.getElementById('count').value;

    let item = {
        id: id,
        name: name,
        shortName: short_name,
        description: description,
        unit: unit,
        purchasePrice: price,
        category: category,
        manufacturer: manufacturer,
        count: count,
        reserved: 0
    }

    let itemJSON = JSON.stringify(item);

    alert('Подтвердите создание нового товара')

    let http = new XMLHttpRequest()
    let url = '/admin/item'
    http.open('POST', url, false)
        http.setRequestHeader('Content-Type', 'application/json');
    http.send(itemJSON)

    if (http.status === 200) {
        alert('Товар успешно добавлен')
        showCreateItemPage()
    } else if (http.status === 500 && JSON.parse(http.responseText).errorCode === "ITEM_ALREADY_EXISTS") {
        alert('Товар с таким ID уже существует')
        document.getElementById('id').style.color = "red";
        document.getElementById('id').style.background = "LightYellow"

        // JSON.parse(http.responseText, function (key, value) {
        //     if (key === 'errorCode' && value === "ITEM_ALREADY_EXISTS") {
        //         alert('Товар с таким ID уже существует')
        //         document.getElementById('id').style.color = "red";
        //         document.getElementById('id').style.background = "LightYellow"
        //     }
        // })
    } else {
        alert('Произошла ошибка при добавлении товара')
    }
}
