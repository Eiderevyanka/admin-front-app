package ru.reboot.price.service;

import org.junit.Assert;
import org.junit.Test;
import ru.reboot.dto.ItemInfo;
import ru.reboot.dto.PriceInfo;
import ru.reboot.service.AdminService;
import ru.reboot.service.PriceCache;

import java.util.List;

public class CachePriceTest {

    private final String NEW_ITEM = "new item";
    private final Double PRICE = 10.0;

    private PriceCache priceCache = new PriceCache();



    @Test
    public void putAndGetPriceTest() {
        String NEW_ITEM = "new item1";
        Double PRICE = 10.0;

        Assert.assertNull(priceCache.get(NEW_ITEM));
        priceCache.putPrice(NEW_ITEM, PRICE);
        Assert.assertEquals(PRICE, priceCache.get(NEW_ITEM));
    }

    @Test
    public void containsPriceTest() {
        String NEW_ITEM = "new item2";
        Double PRICE = 10.0;

        Assert.assertFalse(priceCache.contains(NEW_ITEM));
        priceCache.putPrice(NEW_ITEM, PRICE);
        Assert.assertTrue(priceCache.contains(NEW_ITEM));
    }
}
