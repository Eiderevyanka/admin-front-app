package ru.reboot.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;
import ru.reboot.dto.ItemDTO;
import ru.reboot.error.BusinessLogicException;
import ru.reboot.error.ErrorCode;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class CreateItemTest {

    private final ObjectMapper objectMapper = mock(ObjectMapper.class);
    private final RestTemplate restTemplate = mock(RestTemplate.class);
    private final AdminService adminService = new AdminService();
    private final ItemDTO itemDTO = new ItemDTO.Builder().setId("1").build();
    private ItemDTO createdItem;

    {
        adminService.setRestTemplate(restTemplate);
        adminService.setMapper(objectMapper);
    }

    @Test
    public void createItemBadData() throws Exception {

        try {
            createdItem = adminService.createItem(null);
            fail();
        } catch (BusinessLogicException e) {
            assertEquals(e.getCode(), ErrorCode.ILLEGAL_ARGUMENT);
        }

        itemDTO.setId("");
        try {
            createdItem = adminService.createItem(itemDTO);
            fail();
        } catch (BusinessLogicException e) {
            assertEquals(e.getCode(), ErrorCode.ILLEGAL_ARGUMENT);
        }

        itemDTO.setId(null);
        try {
            createdItem = adminService.createItem(itemDTO);
            fail();
        } catch (BusinessLogicException e) {
            assertEquals(e.getCode(), ErrorCode.ILLEGAL_ARGUMENT);
        }

    }

    @Test
    public void createItemFail() throws Exception {

        when(restTemplate.postForObject(anyString(), any(), any()))
                .thenThrow(RuntimeException.class);
        when(objectMapper.writeValueAsString(itemDTO)).thenReturn(anyString());

        try {
            createdItem = adminService.createItem(itemDTO);
            fail();
        } catch (Exception e) {}

        verify(objectMapper).writeValueAsString(itemDTO);
        verify(restTemplate).postForObject(anyString(), any(), any());
    }

    @Test
    public void createItemPositive() throws Exception {

        when(restTemplate.postForObject(anyString(), any(), any()))
                .thenReturn(itemDTO);
        when(objectMapper.writeValueAsString(itemDTO)).thenReturn(anyString());

        ItemDTO createdItem = adminService.createItem(itemDTO);

        assertEquals(itemDTO, createdItem);
        verify(objectMapper).writeValueAsString(itemDTO);
        verify(restTemplate).postForObject(anyString(), any(), any());
    }
}