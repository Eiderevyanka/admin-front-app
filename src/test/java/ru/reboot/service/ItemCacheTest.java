package ru.reboot.service;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import ru.reboot.dto.ItemDTO;
import ru.reboot.dto.ItemInfo;
import ru.reboot.error.BusinessLogicException;

import java.util.*;

public class ItemCacheTest {

    @Test
    public void GetTest_itemIdIsNull() {
        ItemCache itemCache = new ItemCache();

        Assert.assertNull(itemCache.get(""));
        Assert.assertNull(itemCache.get(null));
    }

    @Test
    public void putTest_itemIdIsNull() {
        ItemCache itemCache = new ItemCache();
        ItemDTO itemInfoMock = Mockito.mock(ItemDTO.class);

        try {
            itemCache.put("", itemInfoMock);
            Assert.fail();
        } catch (BusinessLogicException e) {
            Assert.assertEquals(e.getCode(), "ILLEGAL_ARGUMENT");
        }
    }

    @Test
    public void putAndGetTest_positive() {
        ItemDTO itemInfo = new ItemDTO.Builder().setId("someId").setCount(10).build();
        ItemCache itemCache = new ItemCache();

        itemCache.put("someId", itemInfo);

        Assert.assertEquals(itemCache.get("someId").getId(), "someId");
        Assert.assertEquals(itemCache.get("someId").getCount(), 10);

    }

    @Test
    public void getKeysTest_positive() {
        ItemDTO itemInfo = new ItemDTO.Builder().setId("someId").setCount(10).build();
        ItemCache itemCache = new ItemCache();

        itemCache.put("someId", itemInfo);

        Set<String> setKeys = new HashSet<>();
        setKeys.add("someId");

        Assert.assertEquals(setKeys, itemCache.getKeys());
    }

    @Test
    public void getAllItemsTest_itemCacheNotInit() {
        ItemDTO itemInfo = new ItemDTO();
        ItemCache itemCache = new ItemCache();
        AdminService adminService = new AdminService();
        adminService.setItemCache(itemCache);

        Assert.assertEquals(Collections.emptyList(), adminService.getAllItems());
    }

    @Test
    public void getAllItemsTest_positive() {
        ItemDTO itemInfo = new ItemDTO.Builder().setId("someId").setCount(10).build();
        ItemCache itemCache = new ItemCache();
        AdminService adminService = new AdminService();
        adminService.setItemCache(itemCache);
        adminService.setPriceCache(new PriceCache());

        itemCache.put("someId", itemInfo);
        List<ItemInfo> itemInfoList = adminService.getAllItems();

        Assert.assertEquals(itemCache.get("someId").getId(), itemInfoList.get(0).getId());
        Assert.assertEquals(itemCache.get("someId").getCount(), itemInfoList.get(0).getCount());
    }
}
